﻿var port = 3000; // Указываем порт на котором у на стоит сокет
// var socket = io.connect('http://192.168.22.239:' + port); // Тут мы объявляем "socket" (дальше мы будем с ним работать) и подключаемся сразу к серверу через порт
var socket = io.connect('ws://192.168.22.239:' + port); // Тут мы объявляем "socket" (дальше мы будем с ним работать) и подключаемся сразу к серверу через порт


function createImg(name, img ) {
    if (typeof img == "undefined")
    {
        img = "";
    }
    id = name.replace(".", "_");
    cam = name.replace(".jpg", "");
    backgroundClass = name.replace(/-.+/, "").replace(/_.+/, "");
    //console.log(backgroundClass);
    var element = document.getElementById('img-' + id);
    if (!element) {

            url = "http://"+urlArray[cam];

        camName = "";
        if ( typeof camNameArray[cam] !== "undefined")
        {
            camName = camNameArray[cam];
        }
        else
        {
            camName = cam;
        }
        $('div#cameras').append('<span class="image ' + backgroundClass + ' hoverspan" style="width:' + imgW + '; height:' + imgH + '">'

            + '<img width="' + imgW + '" height="' + imgH + '"'
            + 'src="http://192.168.22.239/thumb/' + name + '" '
            + 'id="img-' + id + '" '
            + 'class="preview" '
            + 'onerror="this.src=\'gold.gif\'">'
            + '<h6 class="pic-name">'+camName+'</h6>'
            + '<a onclick="PopUpShow(this)" target=_blank class="left_button btn btn-success" role="button" data-tooltip="' + id + '">'
            + '<span class="glyphicon glyphicon-zoom-in"></span></a>'
            + '<a href="' + url + '" target=_blank class="left_button btn btn-info" '
            + 'role="button" data-tooltip="' + id + '" data-toggle="tooltip" title="Перейти в админку камеры">'
            + '<span class="glyphicon glyphicon-cog"></span></a>'
            //+'<a href="#" class="right_button btn btn-danger" role="button">правая</a>'

            + '</span>');
    }
    else {
        $('#img-' + id).attr("src", img.src);
        $('#img2-' + id).attr("src", img.src);
        //console.log('img-'+id);
    }
};


socket.on('connect', function () {
    console.log('connect establish!');
    socket.emit('username', username);
    socket.emit('files-block', filesBlock);
});

socket.on('files', function (files) {

    for (var i = 0; i < files.length; i++) {
        createImg(files[i]);
        //console.log(files[i]);
    }
});

socket.on("image", function (info) {
    if (info.image) {
        var img = new Image();
        img.src = 'data:image/jpeg;base64,' + info.buffer;
        createImg(info.filename, img);
    }
});

$(document).ready(function () {
    PopUpHide();
});

function PopUpShow(e) {

    data_tooltip = e.getAttribute("data-tooltip");
    src = 'http://192.168.22.239/' + data_tooltip.replace(/_jpg/, ".jpg");
    if ( typeof camNameArray[data_tooltip.replace(/_jpg/, "")] !== "undefined")
    {
        camName = camNameArray[data_tooltip.replace(/_jpg/, "")];
    }
    else
    {
        camName = data_tooltip.replace(/_jpg/, "");
    }

    $('h2.popup-img').replaceWith(
        '<h2 class="popup-img">'+camName+'</h2>'
    );
    $('img.popup-img').replaceWith('<img class="popup-img" src="' + src + '" width="800" align="center" id="img2-' + data_tooltip + '" onerror="this.src=\'gold.gif\'">');
    $("#popup1").show();
}
function PopUpHide() {
    $('img.popup-img').replaceWith('<img class="popup-img">');
    $("#popup1").hide();
}

$(document).mouseup(function (e) {
    var container = $("div.b-popup-content");
    if (container.has(e.target).length === 0) {
        PopUpHide();
        //container.hide();
    }
});

function actionOnClickAdmin(button) {
    console.log("click on " + "http://localhost:61337/" + datasheet[button.variable + '.jpg']['url'].replace('http://', ""));
    window.open("http://localhost:61337/" + datasheet[button.variable + '.jpg']['url'].replace('http://', ""));
    // this.key();
}

function msieversion() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        return true;
        //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    }
    else  // If another browser, return 0
    {
        //alert('otherbrowser');
        return false;
    }

    return false;
}