<?php

return array(
    'type'      => 'group',
    'defaults'  => array('action' => 'default'),
    'resolvers' => array(
        'preset' => array(
            'path' => 'preset/<id>',
            'defaults' => array('processor' => 'videowall', 'action' => 'default')
        ),
        'action' => array(
            'path' => '<processor>/<action>/(<id>)'
        ),
        'processor' => array(
            'path'     => '(<processor>)',
            'defaults' => array('processor' => 'videowall')
        ),
        'frontpage' => array(
            'path' => '',
            'defaults' => ['processor' => 'videowall']
        )
    )
);
