<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 24.05.2017
 * Time: 14:20
 */
$this->layout('app:layout'); ?>
<h2>Список пресетов</h2>
<ul>
    <li>Можно вынести свои настройки в пресет.</li>
    <li>Переписывать пресеты нельзя.</li>
    <li>Удалять пресеты может только администратор.</li>
</ul>
<ul>
    <?php foreach ($presets as $preset): ?>
        <li><p>
            <a href="/preset/<?= $preset->username ?>">
                <h3><?= $preset->username ?></h3>
            </a>
            <a href="/settings/copyToYouself/<?= $preset->username ?>">
                Скопировать себе
            </a>
            </p>
        </li>
    <?php endforeach; ?>
</ul>
