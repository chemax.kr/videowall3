<?php $this->layout('app:layout');?>

<script>
    var username = '<?=$username?>';
    var imgW = <?=$imgw?>;
    var imgH = <?=$imgh?>;
    <?php if($userSettings==""):?>
    var filesList = [];
    <?php else:?>
    var filesList = <?=$userSettings?>;
    <?php endif;?>
    var urlArray = <?=$urlArray?>;
    var camNameArray = <?=$camNameArray?>;
</script>
<script src="/main.js" charset="utf-8"></script>

<div id="checkboxes"></div>
<div align="center" id="cameras" style="display:block-inline" >

</div>
<div class="b-popup" id="popup1">
    <div class="b-popup-content">
        <h2 class="popup-img"></h2>
        <a href="javascript:PopUpHide()" role="button" class="btn btn-danger" align="right"><span class="glyphicon glyphicon-zoom-out"></span></a>
        <img class="popup-img">
    </div>