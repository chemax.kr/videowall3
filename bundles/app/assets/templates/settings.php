<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 23.05.2017
 * Time: 9:56
 */
$areaArray = "";
$this->layout('app:layout'); ?>
<h2>Отметьте камеры, которые желаете видеть.</h2>
<form action="/settings/save/" method="post">

    <p><input type="submit" role="button" class="btn btn-success" >
        <a href="/" class="btn btn-danger" role="button">Отмена</a>
        <a onclick="uncheckAll()" class="btn btn-danger" role="button">Снять всё</a>
        <a onclick="checkAll()" class="btn btn-info" role="button">Отметить всё</a>
        <a onclick="invertCheck()" class="btn btn-info" role="button">Инвертировать</a>
    </p>

    <div>
        <div><label>Ширина изображений:</label><input type="number" name="imgw" value="<?=$imgw?>"></div>
        <div><label>Высота изображений:</label><input type="number" name="imgh" value="<?=$imgh?>"></div>
    </div>

    <div class="column">
        <ul id="list">
            <?php $camNameList = array_keys($orderedCamList);
            foreach ($camNameList as $cam):?>
            <li>
                <input id="<?=$cam?>" type="checkbox" name="camSelect[<?= $cam ?>]" <?=$orderedCamList[$cam]?> >

                    <?php if (isset($camArray[$cam])): ?>
                        <?= $camArray[$cam]->area()->name ?>
                        <?= $camArray[$cam]->place ?>
                    <?php else: ?>
                        <?= $cam ?>

                    <?php endif; ?>

            </li>
                <?php
                $area = preg_replace('/_.+/',"",preg_replace('/-.+/',"",$cam));
                $areaArray[$area]="";
                ?>
            <?php endforeach;?>
        </ul>
    </div>


    <p><input type="submit" role="button" class="btn btn-success" >
        <a href="/" class="btn btn-danger" role="button">Отмена</a>
        <a onclick="uncheckAll()" class="btn btn-danger" role="button">Снять всё</a>
        <a onclick="checkAll()" class="btn btn-info" role="button">Отметить всё</a>
        <a onclick="invertCheck()" class="btn btn-info" role="button">Инвертировать</a>
    </p>
</form>

<h3>Отметить весь участок:</h3>
<?php foreach(array_keys($areaArray) as $area):?>
    <a name="<?=$area?>" role="button" class="btn btn-info" onclick="uncheckArea(this)"><?=$area?></a>
<?php endforeach;?>
<p>

<form action="/settings/savePreset/" method="get">
    <input name="presetName" type="text">
    <input type="submit" role="button" class="btn btn-success" value="Создать пресет на основе действующих настроек.">
</form>
</p>

<script>
    function sortable(rootEl, onUpdate){
        var dragEl;

        // Делаем всех детей перетаскиваемыми
        [].slice.call(rootEl.children).forEach(function (itemEl){
            itemEl.draggable = true;
        });

        // Фнукция отвечающая за сортировку
        function _onDragOver(evt){
            evt.preventDefault();
            evt.dataTransfer.dropEffect = 'move';

            var target = evt.target;
            if( target && target !== dragEl && target.nodeName == 'LI' ){
                // Сортируем
                rootEl.insertBefore(dragEl, target.nextSibling || target);
            }
        }

        // Окончание сортировки
        function _onDragEnd(evt){
            evt.preventDefault();

            dragEl.classList.remove('ghost');
            rootEl.removeEventListener('dragover', _onDragOver, false);
            rootEl.removeEventListener('dragend', _onDragEnd, false);

            // Сообщаем об окончании сортировки
            onUpdate(dragEl);
        }

        // Начало сортировки
        rootEl.addEventListener('dragstart', function (evt){
            dragEl = evt.target; // Запоминаем элемент который будет перемещать

            // Ограничиваем тип перетаскивания
            evt.dataTransfer.effectAllowed = 'move';
            evt.dataTransfer.setData('Text', dragEl.textContent);

            // Пописываемся на события при dnd
            rootEl.addEventListener('dragover', _onDragOver, false);
            rootEl.addEventListener('dragend', _onDragEnd, false);

            setTimeout(function (){
                // Если выполнить данное действие без setTimeout, то
                // перетаскиваемый объект, будет иметь этот класс.
                dragEl.classList.add('ghost');
            }, 0)
        }, false);
    }

    // Используем
    sortable( document.getElementById('list'), function (item){
        console.log(item);
    });
    var uncheckArea = function (e) {
      console.log(e.getAttribute('name'));
      name = e.getAttribute('name');
      $('[id^="'+name+'"]').prop( "checked", true );
    };
    var uncheckAll = function () {
        $('[name^="camSelect"]').prop( "checked", false );
    };
    var checkAll = function () {
        $('[name^="camSelect"]').prop( "checked", true );
    };
    var invertCheck = function () {
        $('[name^="camSelect"]').each( function() {
            $(this).attr('checked', !$(this).attr('checked'));
        });
        //$('[name^="camSelect"]').prop( "checked", !$(this).attr('checked') );
    };
</script>
