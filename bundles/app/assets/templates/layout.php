<html>
<head>
    <meta charset="utf-8">
    <title>Видеостена 2</title>
    <script src="/socket.io.js"></script>
    <script src="/delivery.js"></script>
    <script src="/js/jquery-3.1.0.min.js" charset="utf-8"></script>

    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css"/>
    <script src="/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/main.css"/>
	<link rel="stylesheet" href="/js/colorbox/colorbox.css" />
	<script src="/js/colorbox/jquery.colorbox.js"></script>
	
</head>
<body>
<p align="right" id="main_menu">
    <a href="/settings/presetlist/"><span class="glyphicon glyphicon-th" data-toggle="tooltip"
                              title="Пресеты"></a>
    <a href="/settings"><span class="glyphicon glyphicon-cog" data-toggle="tooltip"
                              title="Перейти в раздел настроек"></a></p>
<?php $this->childContent(); ?>

</body>
</html>


		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".slideshow").colorbox({
					current:"Камера {current} из {total}",
					overlayClose: true,
					maxHeight:'90%',
					rel:'slideshow'
					});
			});
		</script>