<?php

namespace Project\App;

class HTTP extends \PHPixie\DefaultBundle\HTTP
{
    protected $classMap = array(
        'greet' => 'Project\App\HTTP\Greet',
        'settings' => 'Project\App\HTTP\Settings',
        'videowall' => 'Project\App\HTTP\Videowall'
    );
}