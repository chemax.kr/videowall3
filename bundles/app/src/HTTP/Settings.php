<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 23.05.2017
 * Time: 9:54
 */
namespace Project\App\HTTP;

use PHPixie\HTTP\Request;

/**
 * Simple greeting web page
 */
class Settings extends Processor
{
    /**
     * Default action
     * @param Request $request HTTP request
     * @return mixed
     */
    public function defaultAction($request)
    {
        $template = $this->components()->template();

        $container = $template->get('app:settings');
        $container->screens = "";
        $namesTmp = (json_decode(file_get_contents('http://192.168.22.239/thumb-json.php')));
        unset($namesTmp[0]);
        unset($namesTmp[1]);
        foreach ($namesTmp as $item) {
            $names[] = preg_replace('~\.jpg~', '', $item);
        }
        $camArray = $this->components()->orm()->query('camera')->find()->asArray(false, 'name');


        $username = $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'];
        $container->username = $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'];
        $userSettings = $this->components()->orm()->query('userSetting')->where('username', $username)->findOne();
        $container->userSettings = "";
        if (($userSettings) && ($userSettings->settings != 'null')) {
            foreach (json_decode($userSettings->settings) as $key => $value) {
                $settings[$key] = $value;
            }
            $container->userSettings = $settings;

        }
        else
        {
            $repoSett = $this->components()->orm()->repository('userSetting');
            $userSettings = $repoSett->create(array(
                'settings' => 'null',
                'username' => $username,
                'imgw' => 200,
                'imgh' => 250
            ));
        }

        $orderedCamList = "";
        if (($userSettings) && ($userSettings->settings != 'null')) {
            foreach (json_decode($userSettings->settings) as $key => $value) {
                $orderedCamList[$key] = "checked";
            }
        }
        foreach ($namesTmp as $item)
        {
            if(!isset($orderedCamList[preg_replace('~\.jpg~', '', $item)]))
            $orderedCamList[preg_replace('~\.jpg~', '', $item)] = "";
        }
        $container->orderedCamList = $orderedCamList;
        $container->imgw = 200;
        $container->imgh = 150;
        if (($userSettings) && ($container->imgw)) {
            $container->imgw = $userSettings->imgw;
            $container->imgh = $userSettings->imgh;
        }
        $container->camArray = $camArray;
//        $container->normalNames = $normalNames;
        $container->camList = $names;

        return $container;
    }

    public function savePresetAction($request)
    {
        print_r($_GET);
        $presetName = $request->query()->get('presetName');
        $orm = $this->components()->orm();
        $userSettings = $this->components()->orm()->query('userSetting')->where('username', $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'])->findOne();
        $repo = $this->components()->orm()->repository('userSetting');
        $repo->create(['username' => $presetName,
            'settings' => $userSettings->settings,
            'imgw' => $userSettings->imgw,
            'imgh' => $userSettings->imgh
        ])->save();
        return $this->redirect('app.frontpage');
//        return "";
    }

    public function presetListAction($request)
    {
        $template = $this->components()->template();
        $container = $template->get('app:preset-list');
        $container->presets = $this->components()->orm()->query('userSetting')->find();
        return $container;

    }

    public function copyToYouselfAction($request)
    {

        $user = $this->components()
            ->orm()->query('userSetting')
            ->where('username', $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'])
            ->findOne();

        $preset = $this->components()
            ->orm()->query('userSetting')
            ->where('username', $request->attributes()->get('id'))
            ->findOne();
        if(!$user)
        {
            $userRepo = $this->components()->orm()->repository('userSetting');
            $user = $userRepo->create([
                'username' => $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'],
                'imgw' => $preset->imgw,
                'imgh' => $preset->imgh,
                'settings' => $preset->settings
            ]);

        }
        else
        {
            $user->imgw = $preset->imgw;
            $user->imgh = $preset->imgh;
            $user->settings = $preset->settings;
        }

        $user->save();
        return $this->redirect('app.processor', array('processor'=>'settings'));
    }

    public function saveAction($request)
    {
        print_r($_POST);
        $query = $this->components()->http()->request()->data();
        $camSelect = $query->get('camSelect');
        $settingRepo = $this->components()->orm()->repository('userSetting');
        $username = $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'];
        if ($request->attributes()->get('id')) {
            $username = $request->attributes()->get('id');
        }
        $userSettings = $this->components()->orm()->query('userSetting')->where('username', $username)->findOne();
        if ($userSettings) {
            $userSettings->settings = json_encode($camSelect);
            $userSettings->imgw = $query->get('imgw');
            $userSettings->imgh = $query->get('imgh');
            $userSettings->save();

        } else {
            $settingRepo->create([
                'username' => $username,
                'settings' => json_encode($camSelect),
                'imgw' => $query->get('imgw'),
                'imgh' => $query->get('imgh')
            ])->save();
        }

        return $this->redirect('app.frontpage');
    }

}