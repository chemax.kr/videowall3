<?php

namespace Project\App\HTTP;

use PHPixie\HTTP\Request;

/**
 * Simple greeting web page
 */
class Videowall extends Processor
{
    /**
     * Default action
     * @param Request $request HTTP request
     * @return mixed
     */
    public function defaultAction($request)
    {
        //$query = $request->query();
        //print_r($_GET);
        $template = $this->components()->template();
        $camArray = $this->components()->orm()->query('camera')->find()->asArray(false, 'name');
        $urlArray = "";
        $camNameArray = "";
        foreach ($camArray as $cam) {
            $urlArray[$cam->name] = preg_replace("~http://~", "", $cam->urlAdmin);
            $camNameArray[$cam->name] = "[" . $cam->area()->name . "] " . $cam->place;
        }
        $container = $template->get('app:greet');
        $container->camNameArray = json_encode($camNameArray);
        $container->urlArray = json_encode($urlArray);
        $container->userSettings = "";
        $container->imgw = 200;
        $container->imgh = 150;
        $username = $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'];
        if ($request->attributes()->get('id')) {
            $username = $request->attributes()->get('id');
        }

        $container->username = $username;


        $userSettings = $this->components()->orm()->query('userSetting')->where('username', $username)->findOne();
        if (($userSettings) && ($userSettings->settings != 'null')) {
            foreach (json_decode($userSettings->settings) as $key => $value) {
                $settings[$key] = $value;
            }

            $container->userSettings = json_encode(array_keys($settings));
        } else {
            $cams = json_decode(file_get_contents('http://192.168.22.239/thumb-json.php'));
            unset($cams[0]);
            unset($cams[1]);
            foreach ($cams as $item) {
                $tmpArr[] = preg_replace('~\.jpg~', '', $item);
            }

            $container->userSettings = json_encode($tmpArr);
        }
        if (($userSettings) && ($container->imgw)) {
            $container->imgw = $userSettings->imgw;
            $container->imgh = $userSettings->imgh;
        }


        return $container;
    }

    public function presetAction($request)
    {
        //return $this->defaultAction($request);

    }
}